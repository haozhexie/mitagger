def main():
    allFiles = list()
    f = open('UnImported.txt', 'r')
    newF = open('newUnImported.txt', 'w')
    for l in f.readlines():
        if l.startswith('N') and (not l in allFiles):
           newF.write(l)
           allFiles.append(l)
    newF.close()

if __name__ == '__main__':
    main()
