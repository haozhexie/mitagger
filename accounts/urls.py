from django.conf import settings
from django.conf.urls import patterns, include, url
from django.conf.urls.static import static

from accounts import views

urlpatterns = patterns('',
    url(r'^$', views.indexView),
    url(r'^login$', views.loginAction),
    url(r'^logout$', views.logoutAction),
    url(r'^admin', views.adminView),
    url(r'^present$', views.presentAction),
    url(r'^addUser$', views.addUserAction),
    url(r'^removeUser$', views.removeUserAction),
    url(r'^save$', views.saveAction),
    url(r'^userPassword$', views.getPasswordAction),
    url(r'^assignImages$', views.assignImageAction),
    url(r'^assignReviewerImages$', views.assignReviewerImageAction),
    url(r'^imageData$', views.getImageCountData),
    url(r'^userimageData$', views.getUserImageCount),
    url(r'^withdraw$', views.withdrawUserImage)
)
