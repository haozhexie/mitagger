from django.contrib import admin
from accounts.models import User

# Register your models here.
class UserAdmin(admin.ModelAdmin):
	"""
	Administration service of User Model.
	The service is provided by Django itself.
	"""
	fields = ['username', 'password']

admin.site.register(User, UserAdmin)