from django.db import models

class User(models.Model):
    username = models.CharField(max_length=16, primary_key=True)
    password = models.CharField(max_length=32)
    isAdministrator = models.BooleanField(default=False)
    isReviewer = models.BooleanField(default=False)

    def __unicode__(self):
        """
        Return the username of the User object.

        @tyoe self: User
        @param self: the User object itself
        @rtype: string
        @return the username of the user.
        """
    	return self.username

    class Meta:
        db_table = "mitagger_users"