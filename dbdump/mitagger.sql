-- phpMyAdmin SQL Dump
-- version 4.2.10.1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: 2014-12-07 21:46:21
-- 服务器版本： 5.6.16
-- PHP Version: 5.5.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `mitagger`
--

-- --------------------------------------------------------

--
-- 表的结构 `auth_group`
--

CREATE TABLE IF NOT EXISTS `auth_group` (
`id` int(11) NOT NULL,
  `name` varchar(80) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- 表的结构 `auth_group_permissions`
--

CREATE TABLE IF NOT EXISTS `auth_group_permissions` (
`id` int(11) NOT NULL,
  `group_id` int(11) NOT NULL,
  `permission_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- 表的结构 `auth_permission`
--

CREATE TABLE IF NOT EXISTS `auth_permission` (
`id` int(11) NOT NULL,
  `name` varchar(50) NOT NULL,
  `content_type_id` int(11) NOT NULL,
  `codename` varchar(100) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=52 DEFAULT CHARSET=utf8;

--
-- 转存表中的数据 `auth_permission`
--

INSERT INTO `auth_permission` (`id`, `name`, `content_type_id`, `codename`) VALUES
(1, 'Can add log entry', 1, 'add_logentry'),
(2, 'Can change log entry', 1, 'change_logentry'),
(3, 'Can delete log entry', 1, 'delete_logentry'),
(4, 'Can add permission', 2, 'add_permission'),
(5, 'Can change permission', 2, 'change_permission'),
(6, 'Can delete permission', 2, 'delete_permission'),
(7, 'Can add group', 3, 'add_group'),
(8, 'Can change group', 3, 'change_group'),
(9, 'Can delete group', 3, 'delete_group'),
(10, 'Can add user', 4, 'add_user'),
(11, 'Can change user', 4, 'change_user'),
(12, 'Can delete user', 4, 'delete_user'),
(13, 'Can add content type', 5, 'add_contenttype'),
(14, 'Can change content type', 5, 'change_contenttype'),
(15, 'Can delete content type', 5, 'delete_contenttype'),
(16, 'Can add session', 6, 'add_session'),
(17, 'Can change session', 6, 'change_session'),
(18, 'Can delete session', 6, 'delete_session'),
(19, 'Can add user', 7, 'add_user'),
(20, 'Can change user', 7, 'change_user'),
(21, 'Can delete user', 7, 'delete_user'),
(22, 'Can add article', 8, 'add_article'),
(23, 'Can change article', 8, 'change_article'),
(24, 'Can delete article', 8, 'delete_article'),
(25, 'Can add tag type', 9, 'add_tagtype'),
(26, 'Can change tag type', 9, 'change_tagtype'),
(27, 'Can delete tag type', 9, 'delete_tagtype'),
(28, 'Can add ontology', 10, 'add_ontology'),
(29, 'Can change ontology', 10, 'change_ontology'),
(30, 'Can delete ontology', 10, 'delete_ontology'),
(31, 'Can add image', 11, 'add_image'),
(32, 'Can change image', 11, 'change_image'),
(33, 'Can delete image', 11, 'delete_image'),
(34, 'Can add tag', 12, 'add_tag'),
(35, 'Can change tag', 12, 'change_tag'),
(36, 'Can delete tag', 12, 'delete_tag'),
(37, 'Can add tag relationship', 13, 'add_tagrelationship'),
(38, 'Can change tag relationship', 13, 'change_tagrelationship'),
(39, 'Can delete tag relationship', 13, 'delete_tagrelationship'),
(40, 'Can add image tag', 14, 'add_imagetag'),
(41, 'Can change image tag', 14, 'change_imagetag'),
(42, 'Can delete image tag', 14, 'delete_imagetag'),
(43, 'Can add annotations', 15, 'add_annotations'),
(44, 'Can change annotations', 15, 'change_annotations'),
(45, 'Can delete annotations', 15, 'delete_annotations'),
(46, 'Can add image assignments', 16, 'add_imageassignments'),
(47, 'Can change image assignments', 16, 'change_imageassignments'),
(48, 'Can delete image assignments', 16, 'delete_imageassignments'),
(49, 'Can add recommendations', 17, 'add_recommendations'),
(50, 'Can change recommendations', 17, 'change_recommendations'),
(51, 'Can delete recommendations', 17, 'delete_recommendations');

-- --------------------------------------------------------

--
-- 表的结构 `auth_user`
--

CREATE TABLE IF NOT EXISTS `auth_user` (
`id` int(11) NOT NULL,
  `password` varchar(128) NOT NULL,
  `last_login` datetime NOT NULL,
  `is_superuser` tinyint(1) NOT NULL,
  `username` varchar(30) NOT NULL,
  `first_name` varchar(30) NOT NULL,
  `last_name` varchar(30) NOT NULL,
  `email` varchar(75) NOT NULL,
  `is_staff` tinyint(1) NOT NULL,
  `is_active` tinyint(1) NOT NULL,
  `date_joined` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- 表的结构 `auth_user_groups`
--

CREATE TABLE IF NOT EXISTS `auth_user_groups` (
`id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `group_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- 表的结构 `auth_user_user_permissions`
--

CREATE TABLE IF NOT EXISTS `auth_user_user_permissions` (
`id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `permission_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- 表的结构 `django_admin_log`
--

CREATE TABLE IF NOT EXISTS `django_admin_log` (
`id` int(11) NOT NULL,
  `action_time` datetime NOT NULL,
  `user_id` int(11) NOT NULL,
  `content_type_id` int(11) DEFAULT NULL,
  `object_id` longtext,
  `object_repr` varchar(200) NOT NULL,
  `action_flag` smallint(5) unsigned NOT NULL,
  `change_message` longtext NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- 表的结构 `django_content_type`
--

CREATE TABLE IF NOT EXISTS `django_content_type` (
`id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `app_label` varchar(100) NOT NULL,
  `model` varchar(100) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=utf8;

--
-- 转存表中的数据 `django_content_type`
--

INSERT INTO `django_content_type` (`id`, `name`, `app_label`, `model`) VALUES
(1, 'log entry', 'admin', 'logentry'),
(2, 'permission', 'auth', 'permission'),
(3, 'group', 'auth', 'group'),
(4, 'user', 'auth', 'user'),
(5, 'content type', 'contenttypes', 'contenttype'),
(6, 'session', 'sessions', 'session'),
(7, 'user', 'accounts', 'user'),
(8, 'article', 'home', 'article'),
(9, 'tag type', 'home', 'tagtype'),
(10, 'ontology', 'home', 'ontology'),
(11, 'image', 'home', 'image'),
(12, 'tag', 'home', 'tag'),
(13, 'tag relationship', 'home', 'tagrelationship'),
(14, 'image tag', 'home', 'imagetag'),
(15, 'annotations', 'home', 'annotations'),
(16, 'image assignments', 'home', 'imageassignments'),
(17, 'recommendations', 'home', 'recommendations');

-- --------------------------------------------------------

--
-- 表的结构 `django_session`
--

CREATE TABLE IF NOT EXISTS `django_session` (
  `session_key` varchar(40) NOT NULL,
  `session_data` longtext NOT NULL,
  `expire_date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- 表的结构 `mitagger_annotations`
--

CREATE TABLE IF NOT EXISTS `mitagger_annotations` (
`id` int(11) NOT NULL,
  `tag_id` int(11) NOT NULL,
  `keyword` varchar(999) NOT NULL,
  `annotationsfrom` smallint(5) unsigned NOT NULL,
  `annotationsto` smallint(5) unsigned NOT NULL,
  `matchType` varchar(16) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- 表的结构 `mitagger_articles`
--

CREATE TABLE IF NOT EXISTS `mitagger_articles` (
`id` int(11) NOT NULL,
  `doi` varchar(64) NOT NULL,
  `article_file_location` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- 表的结构 `mitagger_images`
--

CREATE TABLE IF NOT EXISTS `mitagger_images` (
`id` int(11) NOT NULL,
  `file_name` varchar(64) DEFAULT NULL,
  `title` longtext,
  `legend` longtext,
  `doi_link` varchar(256) DEFAULT NULL,
  `label` varchar(16) DEFAULT NULL,
  `relevant_articleDOI` varchar(128) DEFAULT NULL,
  `article_id` int(11) DEFAULT NULL,
  `image_file_location` varchar(100) DEFAULT NULL,
  `tag_maker_id` varchar(16) DEFAULT NULL,
  `is_tagged` tinyint(1) NOT NULL,
  `is_tagging` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- 表的结构 `mitagger_image_assignments`
--

CREATE TABLE IF NOT EXISTS `mitagger_image_assignments` (
`id` int(11) NOT NULL,
  `image_id` int(11) NOT NULL,
  `user_id` varchar(16) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- 表的结构 `mitagger_image_tags`
--

CREATE TABLE IF NOT EXISTS `mitagger_image_tags` (
`id` int(11) NOT NULL,
  `image_id` int(11) NOT NULL,
  `tag_id` int(11) NOT NULL,
  `author_id` varchar(16) NOT NULL,
  `isSelected` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- 表的结构 `mitagger_ontologies`
--

CREATE TABLE IF NOT EXISTS `mitagger_ontologies` (
`id` int(11) NOT NULL,
  `virtual_id` varchar(128) NOT NULL,
  `name` varchar(256) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8;

--
-- 转存表中的数据 `mitagger_ontologies`
--

INSERT INTO `mitagger_ontologies` (`id`, `virtual_id`, `name`) VALUES
(1, 'null', 'Image Type'),
(2, 'http://data.bioontology.org/ontologies/NCIT', 'National Cancer Institute Thesaurus'),
(3, 'http://data.bioontology.org/ontologies/SNOMEDCT', 'Systematized Nomenclature of Medicine - Clinical Terms'),
(4, 'http://data.bioontology.org/ontologies/RXNORM', 'RxNORM'),
(5, 'http://data.bioontology.org/ontologies/GO', 'Gene Ontology'),
(6, 'http://data.bioontology.org/ontologies/FMA', 'Foundational Model of Anatomy'),
(7, 'http://data.bioontology.org/ontologies/PW', 'Pathway Ontology'),
(8, 'http://data.bioontology.org/ontologies/NIFSTD', 'Neuroscience Information Framework (NIF) Standard Ontology'),
(9, 'http://data.bioontology.org/ontologies/CL', 'Cell Ontology'),
(10, 'http://data.bioontology.org/ontologies/CHEBI', 'Chemical Entities of Biological Interest Ontology'),
(11, 'http://data.bioontology.org/ontologies/DOID', 'Human Disease Ontology'),
(12, 'http://data.bioontology.org/ontologies/BAO', 'BioAssay Ontology'),
(13, 'http://data.bioontology.org/ontologies/XCO', 'Experimental Conditions Ontology'),
(14, 'http://data.bioontology.org/ontologies/FBbi', 'Biological Imaging Methods Ontology'),
(15, 'http://data.bioontology.org/ontologies/PR', 'Protein Ontology');

-- --------------------------------------------------------

--
-- 表的结构 `mitagger_recommendations`
--

CREATE TABLE IF NOT EXISTS `mitagger_recommendations` (
`id` int(11) NOT NULL,
  `tag_id` int(11) NOT NULL,
  `image_id` int(11) NOT NULL,
  `annotationsfrom` smallint(5) unsigned NOT NULL,
  `annotationsto` smallint(5) unsigned NOT NULL,
  `matchType` varchar(16) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- 表的结构 `mitagger_tags`
--

CREATE TABLE IF NOT EXISTS `mitagger_tags` (
`id` int(11) NOT NULL,
  `name` varchar(2048) NOT NULL,
  `tag_type_id` int(11) NOT NULL,
  `fullId` varchar(128) NOT NULL,
  `ontology_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- 表的结构 `mitagger_tag_relationship`
--

CREATE TABLE IF NOT EXISTS `mitagger_tag_relationship` (
`id` int(11) NOT NULL,
  `name` varchar(128) NOT NULL,
  `tag_id` int(11) NOT NULL,
  `parent_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- 表的结构 `mitagger_tag_types`
--

CREATE TABLE IF NOT EXISTS `mitagger_tag_types` (
`id` int(11) NOT NULL,
  `tag_type` varchar(64) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

--
-- 转存表中的数据 `mitagger_tag_types`
--

INSERT INTO `mitagger_tag_types` (`id`, `tag_type`) VALUES
(1, 'search'),
(2, 'recommend'),
(3, 'image-type'),
(4, 'null');

-- --------------------------------------------------------

--
-- 表的结构 `mitagger_users`
--

CREATE TABLE IF NOT EXISTS `mitagger_users` (
  `username` varchar(16) NOT NULL,
  `password` varchar(32) NOT NULL,
  `isAdministrator` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `auth_group`
--
ALTER TABLE `auth_group`
 ADD PRIMARY KEY (`id`), ADD UNIQUE KEY `name` (`name`);

--
-- Indexes for table `auth_group_permissions`
--
ALTER TABLE `auth_group_permissions`
 ADD PRIMARY KEY (`id`), ADD UNIQUE KEY `group_id` (`group_id`,`permission_id`), ADD KEY `auth_group_permissions_5f412f9a` (`group_id`), ADD KEY `auth_group_permissions_83d7f98b` (`permission_id`);

--
-- Indexes for table `auth_permission`
--
ALTER TABLE `auth_permission`
 ADD PRIMARY KEY (`id`), ADD UNIQUE KEY `content_type_id` (`content_type_id`,`codename`), ADD KEY `auth_permission_37ef4eb4` (`content_type_id`);

--
-- Indexes for table `auth_user`
--
ALTER TABLE `auth_user`
 ADD PRIMARY KEY (`id`), ADD UNIQUE KEY `username` (`username`);

--
-- Indexes for table `auth_user_groups`
--
ALTER TABLE `auth_user_groups`
 ADD PRIMARY KEY (`id`), ADD UNIQUE KEY `user_id` (`user_id`,`group_id`), ADD KEY `auth_user_groups_6340c63c` (`user_id`), ADD KEY `auth_user_groups_5f412f9a` (`group_id`);

--
-- Indexes for table `auth_user_user_permissions`
--
ALTER TABLE `auth_user_user_permissions`
 ADD PRIMARY KEY (`id`), ADD UNIQUE KEY `user_id` (`user_id`,`permission_id`), ADD KEY `auth_user_user_permissions_6340c63c` (`user_id`), ADD KEY `auth_user_user_permissions_83d7f98b` (`permission_id`);

--
-- Indexes for table `django_admin_log`
--
ALTER TABLE `django_admin_log`
 ADD PRIMARY KEY (`id`), ADD KEY `django_admin_log_6340c63c` (`user_id`), ADD KEY `django_admin_log_37ef4eb4` (`content_type_id`);

--
-- Indexes for table `django_content_type`
--
ALTER TABLE `django_content_type`
 ADD PRIMARY KEY (`id`), ADD UNIQUE KEY `app_label` (`app_label`,`model`);

--
-- Indexes for table `django_session`
--
ALTER TABLE `django_session`
 ADD PRIMARY KEY (`session_key`), ADD KEY `django_session_b7b81f0c` (`expire_date`);

--
-- Indexes for table `mitagger_annotations`
--
ALTER TABLE `mitagger_annotations`
 ADD PRIMARY KEY (`id`), ADD KEY `mitagger_annotations_5659cca2` (`tag_id`);

--
-- Indexes for table `mitagger_articles`
--
ALTER TABLE `mitagger_articles`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `mitagger_images`
--
ALTER TABLE `mitagger_images`
 ADD PRIMARY KEY (`id`), ADD UNIQUE KEY `file_name` (`file_name`), ADD KEY `mitagger_images_e669cc35` (`article_id`), ADD KEY `mitagger_images_3019d4ed` (`tag_maker_id`);

--
-- Indexes for table `mitagger_image_assignments`
--
ALTER TABLE `mitagger_image_assignments`
 ADD PRIMARY KEY (`id`), ADD KEY `mitagger_image_assignments_06df7330` (`image_id`), ADD KEY `mitagger_image_assignments_6340c63c` (`user_id`);

--
-- Indexes for table `mitagger_image_tags`
--
ALTER TABLE `mitagger_image_tags`
 ADD PRIMARY KEY (`id`), ADD UNIQUE KEY `image_id` (`image_id`,`tag_id`,`author_id`), ADD KEY `mitagger_image_tags_06df7330` (`image_id`), ADD KEY `mitagger_image_tags_5659cca2` (`tag_id`), ADD KEY `mitagger_image_tags_e969df21` (`author_id`);

--
-- Indexes for table `mitagger_ontologies`
--
ALTER TABLE `mitagger_ontologies`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `mitagger_recommendations`
--
ALTER TABLE `mitagger_recommendations`
 ADD PRIMARY KEY (`id`), ADD KEY `mitagger_recommendations_5659cca2` (`tag_id`), ADD KEY `mitagger_recommendations_06df7330` (`image_id`);

--
-- Indexes for table `mitagger_tags`
--
ALTER TABLE `mitagger_tags`
 ADD PRIMARY KEY (`id`), ADD KEY `mitagger_tags_187b8853` (`tag_type_id`), ADD KEY `mitagger_tags_fd3031cc` (`ontology_id`);

--
-- Indexes for table `mitagger_tag_relationship`
--
ALTER TABLE `mitagger_tag_relationship`
 ADD PRIMARY KEY (`id`), ADD UNIQUE KEY `tag_id` (`tag_id`,`parent_id`), ADD KEY `mitagger_tag_relationship_5659cca2` (`tag_id`), ADD KEY `mitagger_tag_relationship_410d0aac` (`parent_id`);

--
-- Indexes for table `mitagger_tag_types`
--
ALTER TABLE `mitagger_tag_types`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `mitagger_users`
--
ALTER TABLE `mitagger_users`
 ADD PRIMARY KEY (`username`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `auth_group`
--
ALTER TABLE `auth_group`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `auth_group_permissions`
--
ALTER TABLE `auth_group_permissions`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `auth_permission`
--
ALTER TABLE `auth_permission`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=52;
--
-- AUTO_INCREMENT for table `auth_user`
--
ALTER TABLE `auth_user`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `auth_user_groups`
--
ALTER TABLE `auth_user_groups`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `auth_user_user_permissions`
--
ALTER TABLE `auth_user_user_permissions`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `django_admin_log`
--
ALTER TABLE `django_admin_log`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `django_content_type`
--
ALTER TABLE `django_content_type`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=18;
--
-- AUTO_INCREMENT for table `mitagger_annotations`
--
ALTER TABLE `mitagger_annotations`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `mitagger_articles`
--
ALTER TABLE `mitagger_articles`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `mitagger_images`
--
ALTER TABLE `mitagger_images`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `mitagger_image_assignments`
--
ALTER TABLE `mitagger_image_assignments`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `mitagger_image_tags`
--
ALTER TABLE `mitagger_image_tags`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `mitagger_ontologies`
--
ALTER TABLE `mitagger_ontologies`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=16;
--
-- AUTO_INCREMENT for table `mitagger_recommendations`
--
ALTER TABLE `mitagger_recommendations`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `mitagger_tags`
--
ALTER TABLE `mitagger_tags`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `mitagger_tag_relationship`
--
ALTER TABLE `mitagger_tag_relationship`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `mitagger_tag_types`
--
ALTER TABLE `mitagger_tag_types`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- 限制导出的表
--

--
-- 限制表 `auth_group_permissions`
--
ALTER TABLE `auth_group_permissions`
ADD CONSTRAINT `group_id_refs_id_f4b32aac` FOREIGN KEY (`group_id`) REFERENCES `auth_group` (`id`),
ADD CONSTRAINT `permission_id_refs_id_6ba0f519` FOREIGN KEY (`permission_id`) REFERENCES `auth_permission` (`id`);

--
-- 限制表 `auth_permission`
--
ALTER TABLE `auth_permission`
ADD CONSTRAINT `content_type_id_refs_id_d043b34a` FOREIGN KEY (`content_type_id`) REFERENCES `django_content_type` (`id`);

--
-- 限制表 `auth_user_groups`
--
ALTER TABLE `auth_user_groups`
ADD CONSTRAINT `user_id_refs_id_40c41112` FOREIGN KEY (`user_id`) REFERENCES `auth_user` (`id`),
ADD CONSTRAINT `group_id_refs_id_274b862c` FOREIGN KEY (`group_id`) REFERENCES `auth_group` (`id`);

--
-- 限制表 `auth_user_user_permissions`
--
ALTER TABLE `auth_user_user_permissions`
ADD CONSTRAINT `user_id_refs_id_4dc23c39` FOREIGN KEY (`user_id`) REFERENCES `auth_user` (`id`),
ADD CONSTRAINT `permission_id_refs_id_35d9ac25` FOREIGN KEY (`permission_id`) REFERENCES `auth_permission` (`id`);

--
-- 限制表 `django_admin_log`
--
ALTER TABLE `django_admin_log`
ADD CONSTRAINT `content_type_id_refs_id_93d2d1f8` FOREIGN KEY (`content_type_id`) REFERENCES `django_content_type` (`id`),
ADD CONSTRAINT `user_id_refs_id_c0d12874` FOREIGN KEY (`user_id`) REFERENCES `auth_user` (`id`);

--
-- 限制表 `mitagger_annotations`
--
ALTER TABLE `mitagger_annotations`
ADD CONSTRAINT `tag_id_refs_id_673c0572` FOREIGN KEY (`tag_id`) REFERENCES `mitagger_tags` (`id`);

--
-- 限制表 `mitagger_images`
--
ALTER TABLE `mitagger_images`
ADD CONSTRAINT `tag_maker_id_refs_username_f9161396` FOREIGN KEY (`tag_maker_id`) REFERENCES `mitagger_users` (`username`),
ADD CONSTRAINT `article_id_refs_id_32477d06` FOREIGN KEY (`article_id`) REFERENCES `mitagger_articles` (`id`);

--
-- 限制表 `mitagger_image_assignments`
--
ALTER TABLE `mitagger_image_assignments`
ADD CONSTRAINT `image_id_refs_id_6634d64a` FOREIGN KEY (`image_id`) REFERENCES `mitagger_images` (`id`),
ADD CONSTRAINT `user_id_refs_username_1bd0e477` FOREIGN KEY (`user_id`) REFERENCES `mitagger_users` (`username`);

--
-- 限制表 `mitagger_image_tags`
--
ALTER TABLE `mitagger_image_tags`
ADD CONSTRAINT `tag_id_refs_id_f16c9884` FOREIGN KEY (`tag_id`) REFERENCES `mitagger_tags` (`id`),
ADD CONSTRAINT `author_id_refs_username_1d8a577d` FOREIGN KEY (`author_id`) REFERENCES `mitagger_users` (`username`),
ADD CONSTRAINT `image_id_refs_id_39e62984` FOREIGN KEY (`image_id`) REFERENCES `mitagger_images` (`id`);

--
-- 限制表 `mitagger_recommendations`
--
ALTER TABLE `mitagger_recommendations`
ADD CONSTRAINT `tag_id_refs_id_1ff929f0` FOREIGN KEY (`tag_id`) REFERENCES `mitagger_tags` (`id`),
ADD CONSTRAINT `image_id_refs_id_90454c82` FOREIGN KEY (`image_id`) REFERENCES `mitagger_images` (`id`);

--
-- 限制表 `mitagger_tags`
--
ALTER TABLE `mitagger_tags`
ADD CONSTRAINT `ontology_id_refs_id_efe2c125` FOREIGN KEY (`ontology_id`) REFERENCES `mitagger_ontologies` (`id`),
ADD CONSTRAINT `tag_type_id_refs_id_94787507` FOREIGN KEY (`tag_type_id`) REFERENCES `mitagger_tag_types` (`id`);

--
-- 限制表 `mitagger_tag_relationship`
--
ALTER TABLE `mitagger_tag_relationship`
ADD CONSTRAINT `parent_id_refs_id_14182f51` FOREIGN KEY (`parent_id`) REFERENCES `mitagger_tags` (`id`),
ADD CONSTRAINT `tag_id_refs_id_14182f51` FOREIGN KEY (`tag_id`) REFERENCES `mitagger_tags` (`id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
