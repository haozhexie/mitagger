import xlrd
import os

def findEmptyTitleInXLS(basePath):
    for parentDir, dirnames, filenames in os.walk(basePath):
        for filename in filenames:
            if not filename.endswith('xlsx'):
                continue
            #print 'processing xlsx file %s' % filename
            fullFilename = (parentDir if parentDir.endswith('/') else parentDir + '/') + filename
            medical_excel = xlrd.open_workbook(fullFilename)
            medical_table = medical_excel.sheets()[0]
            nrows = medical_table.nrows
            for i in range(1, nrows):
                imageFilename  = medical_table.cell(i, 0).value
                title = medical_table.cell(i, 2).value
                legend = medical_table.cell(i, 3).value
                if (len(title) == 0 or title == None or title ==" ") and (len(legend) == 0 or legend == None or legend ==" "):
                    print imageFilename

if __name__ == '__main__':
    findEmptyTitleInXLS('/Users/Bowen/Desktop/xlsx/')