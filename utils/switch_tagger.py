import os
import django
import sys

pro_dir = os.getcwd()
sys.path.append(pro_dir)
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "mitagger.settings")

from home.models import ImageAssignments

def switch_tagger():
    ia_list = ImageAssignments.objects.filter(user_id__in=['Olivia', 'dinghuitong'])
    for ia in ia_list:
        if ia.user.username == 'Olivia':
            ia.user_id = 'dinghuitong'
        elif ia.user.username == 'dinghuitong':
            ia.user_id = 'Olivia'
        ia.save()

django.setup()
switch_tagger()