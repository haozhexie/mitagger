import sys
import os.path
import os
import django


pro_dir = os.getcwd()
sys.path.append(pro_dir)
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "mitagger.settings")

from home.models import Image
from home.annotate import finalize

def finalizeEmptyImages():
    emptyListFile = open('emptyTitleFileName', 'r')
    for line in emptyListFile.readlines():
        if line.endswith('\n'):
            line = line[:len(line)-1]
        try:
            imageObj = Image.objects.get(file_name=line)
            finalize(imageObj.id)
        except:
            print 'error in %s' % line

if __name__ == '__main__':
    django.setup()
    finalizeEmptyImages()
