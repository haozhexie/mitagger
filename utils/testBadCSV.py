import csv
import os.path
import os

def findBadCSV(basePath):
    for parentDir, dirnames, filenames in os.walk(basePath):
            for filename in filenames:
                if not filename.endswith('csv'):
                    continue
                newcr = csv.reader(open((parentDir if parentDir.endswith('/') else parentDir + '/') + filename, 'r'))
                try:    
                    for row in newcr:
                        if not row[0].lower().endswith('tif') or len(row[0]) == 0:
                            print filename
                except:
                    print filename

if __name__ == '__main__':
    findBadCSV('images/')
