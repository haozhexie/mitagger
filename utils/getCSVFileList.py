import os.path
import os
import csv

def iterCSVFile(dirPath):
    for parentDir, dirnames, filenames in os.walk(dirPath):
        for filename in filenames:
            if filename.lower().endswith('csv'):
                printFilesInCSV((parentDir if parentDir.endswith('/') else parentDir+'/') + filename)

def printFilesInCSV(filename):
    csv_reader = csv.reader(open(filename, 'r'))
    targetIndex = 0
    for index, row in enumerate(csv_reader):
        if index == 0:
            for j, item in enumerate(row):
                if 'FigFilename' in item or 'Fig_Filename' in item:
                    targetIndex = j
        else:
            print row[targetIndex]

if __name__ == '__main__':
    iterCSVFile('uploads/')
