from wand.image import Image
import os.path
import os
import sys
import django

pro_dir = os.getcwd()
sys.path.append(pro_dir)
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "mitagger.settings")

def getTifFiles(basePath):
    tifFilePath = list()
    for parentDir, dirnames, filenames in os.walk(basePath):
        for filename in filenames:
            if filename.endswith('tif'):
                newPath = (parentDir if parentDir.endswith('/') else parentDir+'/') + filename
                tifFilePath.append(newPath)
    return tifFilePath

def convertFiles(fileList):
    for filepath in fileList:
        filename, suffix = os.path.splitext(filepath)
        print 'converting %s' % filename
        targetFilePath = '%s.jpg'%(filename)
        image = Image(filename = filepath)
        image.format = 'jpg'
        image.save(filename=targetFilePath)

def mainMethod():
    convertFiles(getTifFiles('uploads/'))


if __name__ == '__main__':
    django.setup()
    mainMethod()
