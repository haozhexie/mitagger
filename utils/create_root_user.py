import os.path
import os
import sys
import django

pro_dir = os.getcwd()
sys.path.append(pro_dir)
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "mitagger.settings")

from accounts.models import User
import hashlib

def create_root_user(username, password):
    try:
        ey_password = hashlib.md5(password).hexdigest()
        user = User(username = username, password = ey_password, isAdministrator=True, isReviewer=False)
        user.save()
        return True
    except:
        return False

if __name__ == '__main__':
    django.setup()
    username = raw_input('Please input root user name: ')
    password = raw_input('Please inpur root user password: ')
    res = create_root_user(username, password)
    if res:
        print 'root user created successfully'
    else:
        print 'Error!! Run this script again'