import os
import django
import sys

pro_dir = os.getcwd()
sys.path.append(pro_dir)
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "mitagger.settings")

from home.models import ImageAssignments
from home.models import Image

def get_missed_list(data_file):
    missed_list = []
    for line in data_file:
        missed_list.append(line)
    missed_img_list = Image.objects.filter(file_name__in=missed_list)
    print len(missed_img_list)
    return missed_img_list

def assign_missed_images(missed_list):
    user1 = 'Olivia'
    user2 = 'dinghuitong'
    user1_images = missed_list[:1574]
    user2_images = missed_list[1574:]
    for obj in user1_images:
        new_assign = ImageAssignments(image=obj, user_id=user1)
        new_assign.save()
    for obj in user2_images:
        new_assign = ImageAssignments(image=obj, user_id=user2)
        new_assign.save()

def assign_to_new_user():
    all_ia_list = ImageAssignments.objects.filter(user_id='Olivia')
    ia_list = all_ia_list[len(all_ia_list)-300:len(all_ia_list)]
    for img in ia_list:
        img.delete()
        new_assign = ImageAssignments(image=img.image, user_id='Malik')
        new_assign.save()
    ia_list = ImageAssignments.objects.filter(user_id='dinghuitong')
    ia_list = all_ia_list[len(all_ia_list)-200:len(all_ia_list)]
    for img in ia_list:
        img.delete()
        new_assign = ImageAssignments(image=img.image, user_id='Malik')
        new_assign.save()


if __name__ == '__main__':
    django.setup()
    data_file = open('no_tags.txt')
    assign_to_new_user()
