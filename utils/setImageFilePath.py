import os
import os.path
import sys
import django

pro_dir = os.getcwd()
sys.path.append(pro_dir)
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "mitagger.settings")

from home.models import Image
from home.files import createFolder

def getImagePathDict(basePath):
    pathDict = dict()
    for parentDir, dirnames, filenames in os.walk(basePath):
        for filename in filenames:
            if filename.lower().endswith('tif'):
                fullFilename = (parentDir if parentDir.endswith('/') else parentDir + '/') + filename
                pathDict[filename] = fullFilename

def mainFunc():
    print 'get image dict'
    imageD = getImagePathDict('images/new2/')
    allImage = Image.objects.all()
    newPath = createFolder('image')
    for imageObj in allImage:
        if imageObj.image_file_location == None or imageObj.image_file_location == "":
            shutil.move(imageD['file_name'], basePath)
            imageObj.image_file_location = basePath + imageD['file_name']

if __name__ == '__main__':
    django.setup()
    mainFunc()