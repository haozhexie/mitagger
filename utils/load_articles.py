import os
import django
import sys

pro_dir = os.getcwd()
sys.path.append(pro_dir)
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "mitagger.settings")

from home.models import Article
from home.models import Image
from home.files import saveArticleInfo

def load_article(basepath):
    for parentDir, dirnames, filenames in os.walk(basepath):
        for filename in filenames:
            fullFilename = (parentDir if parentDir.endswith('/') else parentDir + '/') + filename
            saveArticleInfo(fullFilename)

def get_missed_img_captions(image_list):
    for img in image_list:
        if (img.legend == None or img.legend == '') and (img.title == None or img.title == ''):
            ana_related_article(img)

def ana_related_article(img):
    atricle_obj = img.article
    
