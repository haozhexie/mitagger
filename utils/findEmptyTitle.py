import sys
import os.path
import os
import django

pro_dir = os.getcwd()
sys.path.append(pro_dir)
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "mitagger.settings")

from home.models import Image

def findEmptyTitle():
    emptyTitleFile = open('emptyTitleFileName', 'w')
    emptyTitleImages = Image.objects.filter(title='').filter(legend='')

    for image in emptyTitleImages:
        emptyTitleFile.write(image.file_name + '\n')


if __name__ == '__main__':
    django.setup()
    findEmptyTitle()

