usage(){
    echo "Dump database utility usage: "
    echo "    dump_db.sh <MI Tagger root directory>"
    echo ""
    exit
}

ncftpput_which=`which ncftpput`
if [ -z ${ncftpput_which} ]; then
    echo "Can't find ncftp. Please install it first"
    exit
fi

if [ ! $# -eq 1 ]; then
    usage
fi

root_dir=$1
dump_log_dir="${root_dir}/dump_logs"

mkdir -pv "${dump_log_dir}"

echo ""
echo "Dumping database ..."
mysqldump -u root --password=123456 mitagger > "${dump_log_dir}/database.log"

curtime=`date +%Y_%m_%d_%H_%M`
pkg_file=dump_logs_${curtime}.tar.gz
echo ""
echo "Packaing dump result ..."
tar -czf "${pkg_file}" "${dump_log_dir}"

echo ""
echo "Uploading dump result ..."
ncftpput -u ftpuser -p kloud.snamenode.ftpuser -m -R 210.45.241.183 / "${pkg_file}"

echo "Finish dumping!"
echo ""
