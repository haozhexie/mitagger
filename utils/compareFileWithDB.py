import sys
import os.path
import os
import django


pro_dir = os.getcwd()
sys.path.append(pro_dir)
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "mitagger.settings")

from home.models import Image

def compareFileWithDB(imageListFileName, imageFileNameList):
    imageListFile = open(imageListFileName, 'r')
    for filename in imageListFile.readlines():
        if (not (filename in imageFileNameList)) and len(filename) != 0:
            pass 
def mainFunc():
    localImageBasePath = 'newImageInCSV.txt'
    imageFileNameList = list(Image.objects.all().values_list('file_name', flat=True))
    print imageFileNameList[10]
    print len(imageFileNameList)
    compareFileWithDB(localImageBasePath, imageFileNameList)

if __name__ == '__main__':
    django.setup()
    mainFunc()
