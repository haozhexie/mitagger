import os
import django
import sys
from xml.dom import minidom
from django.db.models import Q

pro_dir = os.getcwd()
sys.path.append(pro_dir)
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "mitagger.settings")

from home.models import Tag, Image
from home.models import Ontology
from home.models import Annotations
from home.models import Recommendations
from home.models import ImageTag
from home.models import TagType
from django.db import connection
import csv
import zipfile
import datetime


head_description = ['imageFileName', 'TagID', 'TagName', 'Tag_FullID', 'OntologyName', 'OntologyVirtualID']
head_keys = [
            'imageFileName',
            'tagID',
            'tagName',
            'tag_fullID',
            'ontologyName',
            'ontologyVirtualId',
]

def generateFileName(fileFormat):
    now = datetime.datetime.now()
    fileNameFormat = '%(year)i-%(month)i-%(day)i-%(hour)i-%(minute)i-%(second)i-all.%(extension)s'
    fileNameValue = {
                     'year' : now.year,
                     'month' : now.month,
                     'day' : now.day,
                     'hour' : now.hour,
                     'minute' : now.minute,
                     'second' : now.second,
                     'extension': fileFormat,
                     }
    baseURL = 'downloads/'
    return baseURL + (fileNameFormat % fileNameValue)

def oldsaveAllTagsToCSV():
    cursor = connection.cursor()
    cursor1 = connection.cursor()

    print "Execute cursor1"
    cursor.execute("select distinct images.file_name as imageFileName, tags.id as tagID, tags.name as tagName, tags.fullId as tag_fullId, ontologies.name as ontologyName, ontologies.virtual_id as ontologyVirtualId, tag_types.tag_type as tagTypeName, annotations.matchType as matchType, annotations.annotationsfrom as annotationFrom, annotations.annotationsto as annotationTo " + 
                    "from " +
                    "mitagger_tags tags, mitagger_images images, mitagger_image_tags image_tags, mitagger_ontologies ontologies, mitagger_tag_types tag_types, mitagger_annotations annotations " +
                    "where tags.id = image_tags.tag_id and images.id = image_tags.image_id and annotations.tag_id = tags.id and image_tags.isSelected = 1 and tags.ontology_id = ontologies.id and tag_types.id = tags.tag_type_id order by imageFileName;"
                    )
    print "Execute cursor2"
    cursor1.execute("select distinct images.file_name as imageFileName, tags.id as tagID, tags.name as tagName, tags.fullId as tag_fullId, ontologies.name as ontologyName, ontologies.virtual_id as ontologyVirtualId, tag_types.tag_type as tagTypeName, recommendations.matchType as matchType, recommendations.annotationsfrom as annotationFrom, recommendations.annotationsto as annotationTo " +
                    "from " +
                    "mitagger_tags tags, mitagger_images images, mitagger_image_tags image_tags, mitagger_ontologies ontologies, mitagger_tag_types tag_types, mitagger_recommendations recommendations " +
                    "where tags.id = image_tags.tag_id and images.id = image_tags.image_id and recommendations.tag_id = tags.id and image_tags.isSelected = 1 and tags.ontology_id = ontologies.id and tag_types.id = tags.tag_type_id order by imageFileName;"
                    )
    filename = generateFileName('csv')
    print "Open filei " + filename
    csv_writer = csv.writer(open(filename, 'wb'), delimiter=',')
    print "Write header"
    csv_writer.writerow([i[0] for i in cursor.description])
    print "Write cursor"
    csv_writer.writerows(cursor)
    print "Write cursor1"
    csv_writer.writerows(cursor1)
    cursor.close()
    cursor1.close()
    del csv_writer

    return "Finish"

def saveAllTagsToCSV():
    #get csv writer
    filename = generateFileName('csv')
    csv_writer = csv.writer(open(filename, 'wb'), delimiter=',')

    #get tagged image list
    image_list = Image.objects.filter(file_name__iregex=r'NEJM199[:graph:]*|NEJM20[:graph:]*|NEJM[a-z]+[:graph:]*', is_tagged=True).values_list('id', flat=True)
    #writer header
    #csv_writer.writerow([i for i in getTagsForCurrentImage(image_list[0])[0]])
    csv_writer.writerow(head_keys)

    #get all the tags
    for imageID in image_list:
        print 'Processing image ' + str(imageID)
        tag_info_list = getTagsForCurrentImage(imageID)
        for tag_info in tag_info_list:
            row = list()
            for key in head_keys:
                row.append(tag_info[key])
            csv_writer.writerow(row)

    del csv_writer
    print "Finish"
    return "Finish"

def getImageListAfter1990():
    image_list = Image.objects.filter(file_name__regex=r'NEJM199\w+|NEJM20\w+|NEJM[a-z]+\w+', is_tagged=True).values_list('id', flat=True)
    return image_list

def saveAllTagsToXML():
    #get tagged image list
    image_list = Image.objects.filter(file_name__iregex=r'NEJM199[:graph:]*|NEJM20[:graph:]*|NEJM[a-z]+[:graph:]*', is_tagged=True).values_list('id', flat=True)
    #get xml doc & create root element
    doc  = minidom.Document()
    root = doc.createElement('Data')

    #get all tags & add to xml doc
    for imageID in image_list:
        tag_info_list = getTagsForCurrentImage(imageID)
        for tag_info in tag_info_list:
            tagNode = doc.createElement('Tag')
            print "Write data for image:" + tag_info['imageFileName']
            for key in tag_info:
                infoNode = doc.createElement('%s' % key)
                dataNode = doc.createTextNode('%s' % tag_info[key])
                infoNode.appendChild(dataNode)
                tagNode.appendChild(infoNode)
            root.appendChild(tagNode)
    doc.appendChild(root)
    #get xml str
    xml_str = doc.toxml('utf-8')

    #write to file
    filename = generateFileName('xml')
    xmlFile = open(filename, 'w')
    xmlFile.write(xml_str)
    xmlFile.close()

    #package file
    packageFileName = generateFileName('zip')
    packageFile = zipfile.ZipFile(packageFileName, mode='w', compression=zipfile.ZIP_STORED)
    packageFile.write(filename, getFileRelPath(filename))
    packageFile.close()
    os.remove(filename)

    print "Finish"
    return "Finish"

def getFileRelPath(fileName):
    dirName = os.path.dirname(fileName)
    relPath = os.path.relpath(fileName, dirName)
    
    return relPath

def getTagsForCurrentImage(figureID):
    tag_info_list   = list()
    tagIDList       = ImageTag.objects.filter(image_id=figureID, isSelected=True).values_list("tag_id", flat=True)
    #print "Selected tags: " + str(len(tagIDList))
    annotations     = Annotations.objects.filter(tag_id__in=tagIDList)
    recommendations = Recommendations.objects.filter(tag_id__in=tagIDList, image_id=figureID)
    imageFileName   = Image.objects.get(id=figureID).file_name

    #annotations     = Annotations.objects.filter(tag_id__in=tagIDList)
    #print "annotations: " + str(len(annotations))
    #for annotation in annotations:
    #    tagInfo = {
    #                'imageFileName': imageFileName,
    #                'tagID': annotation.tag.id,
    #                   'tagName': annotation.tag.name,
    #                'tag_fullID': annotation.tag.fullId,
    #                'ontologyName':annotation.tag.ontology.name,
    #                'ontologyVirtualId': annotation.tag.ontology.virtual_id,
    #                'tagTypeName': annotation.tag.tag_type.tag_type,
    #                'matchType': annotation.matchType,
    #                'From': annotation.annotationsfrom,
    #                'To': annotation.annotationsto
    #    }
    #    tag_info_list.append(tagInfo)
    
    recommendations = Recommendations.objects.filter(image_id=figureID, tag_id__in=tagIDList)
    #print "Recommendations tags: " + str(len(recommendations))
    for recommendation in recommendations:
        tagInfo = {
                    'imageFileName': imageFileName,
                    'tagID': recommendation.tag.id,
                    'tagName': recommendation.tag.name,
                    'tag_fullID': recommendation.tag.fullId,
                    'ontologyName':recommendation.tag.ontology.name,
                    'ontologyVirtualId': recommendation.tag.ontology.virtual_id,
        }
        tag_info_list.append(tagInfo)

    return tag_info_list;        

def testAfter1990():
    print 'start'
    image_list = Image.objects.filter(file_name__iregex=r'NEJM199[:graph:]*|NEJM20[:graph:]*|NEJM[a-z]+[:graph:]*').values_list('id', flat=True)
    #image_list = Image.objects.filter(file_name__iregex=r'NEJM194[:graph:]*|NEJM195[:graph:]*|NEJM196[:graph:]*|NEJM197[:graph:]*|NEJM198[:graph:]*|NEJM1990[:graph:]*').values_list('id', flat=True)
    print len(image_list)
    for item in image_list:
        print item

def mainFunc():
    saveAllTagsToCSV()
    saveAllTagsToXML()
   #testAfter1990()

if __name__ == '__main__':
    django.setup()
    mainFunc()
