import os
import django
import sys

pro_dir = os.getcwd()
sys.path.append(pro_dir)
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "mitagger.settings")

from home.images import assignImage

def startAssign(filename):
    assignFile = open(filename,'r')
    for userAssign in assignFile.readlines():
        if userAssign[len(userAssign)-1] == '\n':
            userAssign = userAssign[0:len(userAssign)-1]
        infoList = userAssign.split(' ')
        username = infoList[0]
        count = int(infoList[1])
        assignImage(username, count)


if __name__ == '__main__':
    django.setup()
    startAssign('newAssign.txt')
