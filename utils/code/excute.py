#!/usr/bin/env python
#coding=utf-8

import re
import sys
import os
import platform

from structure import wordStruct

from module import TAGModule
from module import ConfigModule

import extract
import match


#system default setting
reload(sys)
sys.setdefaultencoding('utf-8')

def importData():
	#get file path
	######################################################################################
	global BASE_DIR
	BASE_DIR = os.getcwd()
	#print BASE_DIR
	######################################################################################

	#read the config data
	global config
	config = ConfigModule.Config()
	######################################################################################
	#read the delWord
	if platform.system() == 'Windows':
		delWordPath = BASE_DIR + "\\utlis\\code\\config\\config_del.ini"
	elif platform.system() == 'Linux' or platform.system() == 'Darwin':
		delWordPath = BASE_DIR + "/utils/code/config/config_del.ini"
	print "reading config data of delWord..."
	config.setDelWordDic(delWordPath)
	#print config.getDelWordDic()[ord("a")].getWordQueue()[0].getWordContent()
	print "the data of delWord import finished!"

	##pause code##
	#pause = raw_input("pause")

	#read the weiWord
	print "reading config data of weiWord..."
	if platform.system() == 'Windows':
		weiWordPath = BASE_DIR + "\\code\\config\\config_wei.ini"
	elif platform.system() == 'Linux' or platform.system() == 'Darwin':
		weiWordPath = BASE_DIR + "/utils/code/config/config_wei.ini"
	config.setWeiWordDic(weiWordPath)
	#print config.getWeiWordDic()[ord("b")].getWordQueue()[0].getWordContent()
	print "the data of weiWord import finished!"

	##pause code##
	#pause = raw_input("pause")

	global ontologyPath
	#read OntologyDAT
	print "reading ontology information..."
	if platform.system() == 'Windows':
		ontologyPath = BASE_DIR + "\\resource\\TAG\\ontologies.dat"
	elif platform.system() == 'Linux' or platform.system() == 'Darwin':
		ontologyPath = BASE_DIR + "/utils/resource/TAG/ontologies.dat"
	config.readOntologyDAT(ontologyPath)
	print "the ontology information import finished!"

	##pause code##
	#pause = raw_input("pause")

	#######################################################################################

	##pause code##
	#pause = raw_input("pause")


	#read TAG in tags.dat
	######################################################################################
	global tagWordSeqNoDic
	global TagDATPath
	if platform.system() == 'Windows':
		TagDATPath = BASE_DIR + "\\resource\\TAG\\tags_fixed.dat"
	elif platform.system() == 'Linux' or platform.system() == 'Darwin':
		TagDATPath = BASE_DIR + "/utils/resource/TAG/tags_fixed.dat"
	#TagWordSeqNoDic contains tagSeqNoList and tagNameContent
	tagWordSeqNoDic = TAGModule.TagWordSeqNoDic()
	print "reading DAT file..."
	tagWordSeqNoDic.readDAT(TagDATPath)
	print "DAT file imports finished!"
	##test code##
	"""
	for data in tagWordSeqNoDic.getTagWordSeqNoDic():
		print "###############"
		print data
		print tagWordSeqNoDic.getTagWordSeqNoDic()[data]
		print "#######################"
		pause = raw_input("pause")
	"""
	#print sys.getsizeof(tagWordSeqNoDic.getTagWordSeqNoDic())
	######################################################################################

	##pause code##
	#pause = raw_input("pause")
	
	
	print "all the data import finished!!"
	#######################################################################################

def extractKeyword(caption):
	keyword = extract.Extract(config)
	keyword.setFigCaption(caption)
	keyword.addtionalWordDel()
	keyword.splitWord()
	keyword.setWordWei()
	keyword.deleteWord()
	keyword.addtionalWordWei()
	#keyword.rankWordByScore()
	keyword.filterWord()
	filteredKeywordList = keyword.getFilteredKeywordList()
	#keyword.showFilterWord()
	"""
	print "##############################################################################"
	keyword.showWordContent()
	pause = raw_input("pause")
	"""
	#filteredKeywordList contains  lots of filteredKeyword, which contains the content of keyword and its weight
	return filteredKeywordList


def matchTags(filteredKeywordList, ontologies):
	tags = match.Match(filteredKeywordList, tagWordSeqNoDic.getTagWordSeqNoDic())
	tags.matchKeyword(config)
	tags.gainMatchedTagInformList(TagDATPath, ontologies, config)
	tags.gainShowedTagInformList(config.getOntologyDic())

	#tags.showMatchedTagIndexList()
	#tags.showMatchedTagInformList()
	#tags.showShowedTagInformList()
	
	result = tags.getReturnResult()
	return result

def mainMethod(figCaption, ontologies):
	"""
	######################################################################################
	#use the simulated database to get fig caption
	print "***************simulating the process of database***************"
	print "search database..."
	figCaption = database.getFigCaption(figFileName)
	print "search finished!"
	print "***************simulating the process of database***************"

	if figCaption == None:
		print "the fig filename is illegal!"
		return 0

	######################################################################################
	"""
	print "Extracting the keyword from Fig's caption..."
	#extract the caption of Fig's Keyword
	filteredKeywordList = extractKeyword(figCaption)

	print "Matching the keyword with Tags..."
	#use the keyword list to match the tag
	return matchTags(filteredKeywordList, ontologies)


	