#!/usr/bin/env python
#coding=utf-8

"""
the default weight of each words is 2.0
"""

import csv
import os
import sys

import PUBLICMETHOD


#system default setting
reload(sys)
sys.setdefaultencoding('utf-8')

#word structure
#########################################################################################################
#store each basic word
class Word:

	def __init__(self, wordContent):
		self.__wordContent = wordContent
		self.__wordFirstAlph = ""
		self.__wordTotalWeight = 2.0
		self.__wordFinalWeight = 2.0
		self.__wordChangeTime = 1

	def setWordFirstAlph(self, alph):
		self.__wordFirstAlph = alph

	def getWordFirstAlph(self):
		return self.__wordFirstAlph

	def getWordContent(self):
		return self.__wordContent

	#if there is some rules that change the word's weight, add up them and store it in __wordTotalWeight
	def changeWordWeight(self, weight):
		"""
		if self.__wordContent == "ratio":
			print weight
		"""
		self.__wordTotalWeight += float(weight)
		self.__wordChangeTime += 1
		self.calWordWeight()

	#calculate the average value as the final weight of a word
	def calWordWeight(self):
		"""
		if self.__wordContent == "ratio":
			print self.__wordChangeTime
		"""
		self.__wordFinalWeight = round(self.__wordTotalWeight / self.__wordChangeTime, 3)

	#get the value of word's wei
	def getWordWeight(self):
		return self.__wordFinalWeight

#store some words that will influence other word's weight and class WeightWord inherits from Word
class WeightWord(Word):

	def __init__(self, wordContent):
		Word.__init__(self, wordContent)
		self.__beforeWeight = 2.0
		self.__afterWeight = 2.0
		self.__wordContent = wordContent

	def setWeigh(self, beforeWeight, afterWeight):
		self.__beforeWeight = beforeWeight
		self.__afterWeight = afterWeight

	def getWeightBefore(self):
		return self.__beforeWeight

	def getWeightAfter(self):
		return self.__afterWeight

	def getWordContent(self):
		return self.__wordContent

#store a queue of words(always ranked by their first alphabet)
class WordQueue:

	def __init__(self):
		self.__wordQueue = []
		self.__countWord = 0
		self.__firstAlph = ""

	def deleteWordBySeqNo(self, seqNo):
		#print "the word that be deleted is:" + self.__wordQueue[seqNo].getWordContent()
		del self.__wordQueue[seqNo]
		self.__countWord -= 1

	def changeWordWeightBySeqNo(self, seqNo, weight):
		try:
			self.__wordQueue[seqNo].changeWordWeight(weight)
		except:
			pass

	def setFirstAlph(self, alph):
		self.__firstAlph = alph


	def addWordWithFirstAlph(self, wordContent, alph):
		self.__countWord += 1
		word = Word(wordContent)
		word.setWordFirstAlph(alph)
		self.__wordQueue.append(word)

	def addWord(self, wordContent):
		self.__countWord += 1
		word = Word(wordContent)
		self.__wordQueue.append(word)

	def addWeiWord(self, beforeWeight, afterWeight, wordContent):
		#Merge the function?
		self.__countWord += 1
		weightWord = WeightWord(wordContent)
		weightWord.setWeigh(beforeWeight, afterWeight)
		self.__wordQueue.append(weightWord)

	def getWordQueueContent(self):
		i = 0
		wordContent = []
		while i <  self.__countWord:
			wordContent.append(self.__wordQueue[i].getWordContent())
			i += 1
		return wordContent

	def getCountWord(self):
		return self.__countWord

	def getFirstAlph(self):
		return self.__firstAlph

	def getWordQueue(self):
		return self.__wordQueue

#########################################################################################################