#!/usr/bin/env python
#coding=utf-8
#system default setting
import os
import sys

from module import TAGModule


#system default setting
reload(sys)
sys.setdefaultencoding('utf-8')



class Match:
	def __init__(self, filteredKeywordList, tagWordSeqNoDic):
		self.__filteredKeywordList = filteredKeywordList
		self.__tagWordSeqNoDic = tagWordSeqNoDic
		self.__tagInform = TAGModule.TagInform()

		#the information in self.__matchedTagIndexList contains the words in the matched tag and their wei
		self.__matchedTagWordInformList = []

		#the information in self.__matchedTagInformList contains the list of tag's all information, inclued the tag_id, tag_name, tag_fullId, etc
		self.__matchedTagInformList = []

		#the information in self.__showedTagInformList contaions the list of tag_id, tag_name and the ontology name 
		self.__showedTagInformList = []

	#this is the method which usesd for matching the correct tags with keyword that was extracted before
	def matchKeyword(self, config):
		isFirstTime = True
		matchedTagWordInformList = TAGModule.MatchedTagWordInformList()
		for keyword in self.__filteredKeywordList:
			#print keyword.getContent()
			if keyword.getContent().lower() in self.__tagWordSeqNoDic:
				#print keyword.getContent()
				#pause = raw_input("pause")
				if isFirstTime == True:
					matchedTagWordInformList.constructMatchedTagWordInformList(self.__tagWordSeqNoDic[keyword.getContent().lower()], keyword.getWeight(), len(keyword.getContent()))
					isFirstTime = False
				else:
					matchedTagWordInformList.addMatchedTagWordInform(self.__tagWordSeqNoDic[keyword.getContent().lower()], keyword.getWeight(), len(keyword.getContent()))
		self.__matchedTagWordInformList = matchedTagWordInformList.getMatchedTagWordInformList()
	
	def printMatchedTagWordWeiList(self):
		wordWeiList = self.__matchedTagWordInformList.getMatchedTagWordWeiList()
		for data in wordWeiList:
			print data
		

	def gainMatchedTagInformList(self, DATPath, ontologies, config):
		#get requested ontologies
		filteredTagList = []
		if ontologies == "all" or ontologies == "ALL":
			filteredTagList.append("all")
		else:
			ontologies = ontologies.lower()
			ontologiesList = ontologies.split(',')
			for subscript in config.getOntologyDic().keys():
				for ontology in ontologiesList:
					print config.getOntologyDic()[subscript][1]
					if config.getOntologyDic()[subscript][1].lower() == ontology:
						filteredTagList.append(subscript)

		self.__matchedTagInformList = self.__tagInform.constructAndGainMatchedTagList(self.__matchedTagWordInformList, DATPath, filteredTagList)

	def gainShowedTagInformList(self, ontologyDic):
		self.__showedTagInformList = self.__tagInform.constructShowedTagList(ontologyDic)
		"""
		for showedTag in self.__showedTagInformList:
			print showedTag.getShowedTagName()
		"""


	def getMatchedTagIndexList(self):
		return self.__matchedTagIndexList

	def getMatchedTagInformList(self):
		return self.__matchedTagInformList

	def getShowedTagInformList(self):
		return self.__showedTagInformList


	def showMatchedTagIndexList(self):
		for matchedTagIndex in self.__matchedTagIndexList:
			print matchedTagIndex.getTagName()

	def showMatchedTagInformList(self):
		for matchedTagInform in self.__matchedTagInformList:
			print matchedTagInform

	def showShowedTagInformList(self):
		for showedTagInform in self.__showedTagInformList:
			#print showedTagInform.getShowedTagId() + "\t" + showedTagInform.getShowedTagName() + "\t" + showedTagInform.getOntologyName() + "\t" + showedTagInform.getOntologyLink() + "\t" + showedTagInform.getShowedTagScore()
			print showedTagInform.getShowedTagId() + "\t" + showedTagInform.getShowedTagName() + "\t" + showedTagInform.getShowedTagScore()
			#print showedTagInform.getShowedTagId() + "\t" + showedTagInform.getShowedTagScore()
	
#the result format likes below
# [ 
#   {
#       score: score,
#       annotatedClasses: [
#       {
#           @id: tagId,
#           links: {
#               ontology: ontology_link
#           }
#       },...
#       ]
#   },...
# ]

	def getReturnResult(self):
		result = []
		sameScoreResult = []
		score = 0
		for tag in self.__showedTagInformList:
			if score == tag.getShowedTagScore():
				sameScoreResult.append({
					'@id': tag.getShowedTagId(),
					'links': {
						'ontology': tag.getOntologyLink()
					}
					})
			else:
				#if it's the first time to do this, ignore them
				if sameScoreResult != []:
					result.append({
						'score': score,
						'annotatedClasses': sameScoreResult
						})
					sameScoreResult = []

				score = tag.getShowedTagScore()
				sameScoreResult.append({
					'@id': tag.getShowedTagId(),
					'links': {
						'ontology': tag.getOntologyLink()
					}
					})

		#put the last one tag into result
		result.append({
			'score': score,
			'annotatedClasses': sameScoreResult
			})
		
		return result