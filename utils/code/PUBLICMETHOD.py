#!/usr/bin/env python
#coding=utf-8
import csv
#import os

#This is a file that used to store some public methods, and all of these methods are static method

#read data from file
class ReadFile:

	@classmethod
	def readCSV(self, path):
		self.file = open(path,'rb')
		reader = csv.reader(self.file)
		return reader

	@classmethod
	def readDAT(self, path):
		self.file = open(path,'rb')
		"""
		fileContent = []
		for lineData in self.file:
			fileContent.append(lineData)
		#print id(fileContent)
		"""
		#reader = csv.reader(self.file)
		#The first line is the headline
		#print id(self.file)
		return self.file
		#return reader

	@classmethod
	def readConfig(self, path):
		self.file = open(path)
		fileContent = []
		while 1:
			line = self.file.readline()
			if '#end' in line:
				break
			else:
				fileContent.append(line)
		return fileContent

	@classmethod
	def closeFile(self):
		#print "closed"
		self.file.close()

"""
Path = "E:\\MITagger\\MITagger-searchEngine\\code\\config\\config_del.ini"
reader = readFile.readConfig(Path)
#readFile.readCSV(Path)
for data in reader:
    print data
del reader
readFile.closeFile()
pause = raw_input("pause")
"""
