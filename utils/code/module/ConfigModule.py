#!/usr/bin/env python
#coding=utf-8
#system default setting
import sys
import os

from structure import wordStruct

import PUBLICMETHOD


#system default setting
reload(sys)
sys.setdefaultencoding('utf-8')

#configuration file
#########################################################################################################
#store all the config data including the words should be deleted and the words have their weight, and read the data that store the ontology information
class Config:

	def __init__(self):
		#a dictionary: set up the index between word queue and its first alphabet
		self.__delWordDic = {}
		self.__delWordDicIndexStr = ""
		self.__weiWordDic = {}
		self.__weiWordDicIndexStr = ""
		#a dictionary: set up the index of ontology and it's easy to get its name
		self.__ontologyDic = {}

	#read the file about some words should be deleted
	def setDelWordDic(self, delWordPath):
		lineContent = PUBLICMETHOD.ReadFile.readConfig(delWordPath)
		for line in lineContent:
			isIndexPos = True
			lineData = line.split()
			wordQueue = wordStruct.WordQueue()
			wordQueue.setFirstAlph(lineData[0])
			wordAscii = ord(lineData[0])
			for data in lineData:
				if isIndexPos == True:
					isIndexPos = False
				else:
					wordQueue.addWord(data)
			self.__delWordDicIndexStr += lineData[0]
			self.__delWordDic[wordAscii] = wordQueue
			del lineData
		del lineContent
		PUBLICMETHOD.ReadFile.closeFile()
		##test code##
		"""
		for item in self.__delWordDic:
			print self.__delWordDic[item].getWordQueueContent()
		"""

	def getDelWordDic(self):
		return self.__delWordDic

	def getDelWordDicIndexStr(self):
		return self.__delWordDicIndexStr
		#print self.__delWordDicIndexStr

	#read the file about some words that uses to set up the other word's weight
	def setWeiWordDic(self, weiWordPath):
		lineContent = PUBLICMETHOD.ReadFile.readConfig(weiWordPath)
		for line in lineContent:
			isIndexPos = True
			lineData = line.split()
			wordQueue = wordStruct.WordQueue()
			wordQueue.setFirstAlph(lineData[0])
			wordAscii = ord(lineData[0])
			for data in lineData:
				if isIndexPos == True:
					isIndexPos = False
				else:
					#the least five char are the wei about that word, such as (3,4)
					wordContent = data[:-5]
					#the weight about the word before the weiWord, such as 3 in (3,4)
					beforeWeight = data[-4:-3]
					#the weight about the word after the weiWord, such as 4 in (3,4)
					afterWeight = data[-2:-1]
					wordQueue.addWeiWord(beforeWeight, afterWeight, wordContent)			
			self.__weiWordDicIndexStr += lineData[0]
			self.__weiWordDic[wordAscii] = wordQueue
			del lineData
		del lineContent

		PUBLICMETHOD.ReadFile.closeFile()
		##test code##

		"""
		for item in self.__weiWordDic:
			print self.__weiWordDic[item].getWordQueueContent()
		"""

	def getWeiWordDic(self):
		return self.__weiWordDic

	def getWeiWordDicIndexStr(self):
		return self.__weiWordDicIndexStr

	def readOntologyDAT(self, ontologyPath):
		lineContent = PUBLICMETHOD.ReadFile.readDAT(ontologyPath)

		for line in lineContent:
			#delete "\r" and "\n" in the end of line
			line = line[:-2]
			#print line
			ontologyInform = line.split('\t')
			#ontologyInform = line[0].split('\t')
			#print line
			#print ontologyInform
			self.__ontologyDic[ontologyInform[0]] = [ontologyInform[1], ontologyInform[2]]
		#print self.__ontologyDic[str(11)]
		#print id(lineContent)
		del lineContent
		PUBLICMETHOD.ReadFile.closeFile()

	def getOntologyDic(self):
		return self.__ontologyDic
#########################################################################################################