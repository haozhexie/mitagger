#!/usr/bin/env python
#coding=utf-8
#system default setting
import os
import sys
import platform
sys.path.append("..")

import PUBLICMETHOD


#system default setting
reload(sys)
sys.setdefaultencoding('utf-8')

#Fig
#########################################################################################################
#store the data of each Fig
class Fig:

	def __init__(self):
		self.__fileName = self.__caption = self.__seqNo = self.__title =  ""

	def construct(self, fileName, title, caption, seqNo):
		self.__fileName = fileName
		self.__title = title
		self.__caption = caption
		self.__seqNo = seqNo

	def getFilename(self):
		return self.__fileName

	def getTitle(self):
		return self.__title

	def getCaption(self):
		return self.__caption

	def getSeqNo(self):
		return self.__seqNo

#store all the Fig
class FigList:

	def __init__(self):
		self.__countFig = 0
		self.__figList = []

	def getCountFig(self):
		return self.__countFig

	def getFigList(self):
		return self.__figList

	def addFig(self, filename, title, caption):		
		fig = Fig()
		fig.construct(filename, title, caption, self.__countFig)
		self.__figList.append(fig)
		self.__countFig += 1

	def getFigBySeqNo(self, seqNo):
		#exception handling
		if seqNo > self.__countFig or seqNo <= 0:
			raise ValueError("the seqNo is illegal!")
		else:
			return self.__figList[seqNo]
		
	def getFigCaptionByFileName(self, fileName):
		#exception handling
		try:
			for fig in self.__figList:
				if fig.getFilename() == fileName:
					return fig.getCaption()
		except exception, e:
			raise ValueError("the fileName is illegal!")

	def getFigByFileName(self, fileName):
		#exception handling
		try:
			for fig in self.__figList:
				if fig.getFilename() == fileName:
					return fig
		except exception, e:
			raise ValueError("the fileName is illegal!")

	#read single CSV File
	def readCSVFile(self, CSVPath):
		#call pubilc method
		reader = PUBLICMETHOD.ReadFile.readCSV(CSVPath)
		for lineData in reader:
			self.addFig(lineData[0], lineData[2], lineData[3])
		del reader		
		PUBLICMETHOD.ReadFile.closeFile()

	#read all of CSV which are in one folder
	def readCSVInFolder(self, folderPath):
		self.__folderPath = folderPath
		
		#get each filename in the folder
		filenames = os.listdir(folderPath)
		
		if platform.system() == 'Windows':
			for filename in filenames:
				#print filename
				filePath = folderPath + "\\" + filename
				self.readCSVFile(filePath)
			del filenames
		elif platform.system() == 'Linux':
			for filename in filenames:
				filePath = folderPath + "/" + filename
				self.readCSVFile(filePath)
			del filenames
		
		



"""
#do not use fig index any more
#store one figIndex
class FigIndex:

	def __init__(self, CSVFileName, figFileName):
		self.__CSVFileName = CSVFileName
		self.__figFileName = figFileName

	def getCSVName(self):
		return self.__CSVFileName

	def getFigFileName(self):
		return self.__figFileName

#store all the figId and the index of them, which makes it easy to locate the CSV file that contain the fig
class FigIndexList:

	def __init__(self):
		self.__countFigIndex = 0
		self.__figIndexList = []
		self.__folderPath = ""

	def addFigIndex(self, CSVFileName, figFileName):
		figIndex = FigIndex(CSVFileName, figFileName)
		self.__figIndexList.append(figIndex)
		self.__countFigIndex += 1

	#read all of CSV which are in one folder
	def readCSVInFolder(self, folderPath):
		self.__folderPath = folderPath
		filenames = os.listdir(folderPath)
		#get each filename in the folder
		for filename in filenames:
			#print filename
			filePath = folderPath + "\\" + filename
			reader = PUBLICMETHOD.ReadFile.readCSV(filePath)
			for lineData in reader:
				self.addFigIndex(filename, lineData[0])
			del reader
			PUBLICMETHOD.ReadFile.closeFile()
		del filenames
		

	#return the CSV filename, if no filename was be found, return 0
	def getCSVPathByFigFileName(self, figFileName):
		for figIndex in self.__figIndexList:
			#return figIndex
			if figIndex.getFigFileName() == figFileName:
				return self.__folderPath + "\\" + figIndex.getCSVName()
		return 0

	def getCountFigIndex(self):
		return self.__countFigIndex

	def getFigIndexList(self):
		return self.__figIndexList

"""
#########################################################################################################