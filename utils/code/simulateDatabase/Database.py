#!/usr/bin/env python
#coding=utf-8
import sys
import os
from module import FIGModule
import platform

class Connect:
	def __init__(self):
		self.__figList = FIGModule.FigList()

		#read all CSV Files in a folder
		######################################################################################
		BASE_DIR = os.path.dirname(os.getcwd())
		if platform.system() == 'Windows':
			CSVFolderPath = BASE_DIR + "\\resource\\CSV"
		elif platform.system() == 'Linux':
			CSVFolderPath = BASE_DIR + "/resource/CSV"
		print "***************simulating the process of database***************"
		print "reading all CSV Files..."
		self.__figList.readCSVInFolder(CSVFolderPath)
		print "CSV Files index import finished!"
		print "***************process of database finished!***************"

		##test code##
		"""
		for data in figIndexList.getFigIndexList():
			print data.getFigFileName()
		"""
		##pause code##
		"""
		pause = raw_input("pause")
		return 0
		"""
		######################################################################################

	def getFigCaption(self, figFilename):
		 return self.__figList.getFigCaptionByFileName(figFilename)
