#!/usr/bin/env python

import sys
import os.path
import os
import shutil
import time
import csv
import traceback
import django
import mimetypes

pro_dir = os.getcwd()
sys.path.append(pro_dir)
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "mitagger.settings")


from home.models import Image as MIImage
from home.files import createFolder, getMetaInformation, convertFormat

def mainFunc():
    images = MIImage.objects.all()
    global count
    count = 0
    for image in images:
        filename = image.file_name.replace('tif', 'jpg')
        if not (os.path.exists('uploads/2014/12/14/image/' + filename) or os.path.exists('uploads/2014/12/12/image/' + filename)):
            image.delete()
            print filename
            count = count + 1
    print count


if __name__ == '__main__':
    django.setup()
    mainFunc()
