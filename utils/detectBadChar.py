import sys
import os.path
import os
import django
import csv
import chardet  

pro_dir = os.getcwd()
sys.path.append(pro_dir)
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "mitagger.settings")

def mainFunc():
    csvBasePath = 'uploads'
    for parentDir, dirnames, filenames in os.walk(csvBasePath):
        for filename in filenames:
            if filename.lower().endswith('csv'):
                detectCSV((parentDir if parentDir.endswith('/') else parentDir + '/') + filename)

def detectCSV(filename):
    csvFile = open(filename, 'r')
    for i, line in enumerate(csvFile.readlines()):
        encoding = chardet.detect(line)['encoding']
        if encoding != 'ascii':
            print 'File %s Line %d has error' % (filename, i)


if __name__ == '__main__':
    django.setup()
    mainFunc()
