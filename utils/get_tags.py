import os
import django
import sys

pro_dir = os.getcwd()
sys.path.append(pro_dir)
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "mitagger.settings")

from home.models import Tag

def get_tags(filename):
    res_file = open(filename, 'w')
    res_list = []
    total = 382358
    tag_list = Tag.objects.all()
    count = 0
    print 'process'
    for tag in tag_list:
        if tag.name not in res_list:
            res_list.append(tag.name)
    print 'all tag getted'
    sorted_list = sorted(tag_list)
    for obj in sorted_list:
        res_file.write('%s\n', obj)
        res_file.flush()
    res_file.close()

if __name__ == '__main__':
    print 'started'
    django.setup()
    get_tags('tagbase.txt');
