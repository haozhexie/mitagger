import os
import django
import sys

pro_dir = os.getcwd()
sys.path.append(pro_dir)
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "mitagger.settings")

from home.models import ImageAssignments
from home.models import Image

def get_missed_list(data_file):
    missed_list = []
    for line in data_file:
        missed_list.append(line)
    missed_img_list = Image.objects.filter(file_name__in=missed_list)
    print len(missed_img_list)
    return missed_img_list

def marked_as_untagger(missed_list):
    for img in missed_list:
        img.is_tagged = False
        img.is_tagging = False
        img.save()

def delete_missed(missed_img_list):
    img_assign_objs = ImageAssignments.objects.filter(image__in=missed_img_list)
    for obj in img_assign_objs:
        obj.delete()

if __name__ == '__main__':
    django.setup()
    data_file = open('no_tags.txt');
    delete_missed(get_missed_list(data_file));
