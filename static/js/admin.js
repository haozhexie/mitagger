var tagger_admin = angular.module('tagger_admin', ['ngRoute', 'AdController','ngMaterial','ngAnimate']);

tagger_admin.config(['$routeProvider', function($routeProvider){
	$routeProvider.when('/', {
		templateUrl:'/static/html/admin_main.html',
		controller: 'MainCtrl'
	}).otherwise({
		redirectTo:'/'
	});
}]);



