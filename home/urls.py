from django.conf import settings
from django.conf.urls import patterns, include, url
from django.conf.urls.static import static

from home import views

urlpatterns = patterns('',
    url(r'^$', views.indexView),
    url(r'^import$', views.importView),
    url(r'^dashboard$', views.dashboardView),
    url(r'^images$', views.imagesView),
    url(r'^upload$', views.uploadAction),
    url(r'^getImage$', views.getImageAction),
    url(r'^uploadRelevantArticle$', views.uploadRelevantArticleAction),
    url(r'^getRelevantArticle$', views.getRelevantArticleAction),
    url(r'^getTagsByFigureLegend', views.getTagsByFigureLegendAction),
    url(r'^getTagsByKeyword$', views.getTagsByKeywordAction),
    url(r'^getTagsByGO$', views.getTagsByGOAction),
    url(r'^setImageType$', views.setImageTypeAction),
    url(r'^setTagSelected$', views.setTagSelectedAction),
    url(r'^removeTag$', views.removeTagAction),
    url(r'^export$', views.exportAction),
    url(r'^exportAll$', views.exportAllAction),
    url(r'^downloadBio', views.downloadAction),
    url(r'^finalize', views.finalizeAction),
)
