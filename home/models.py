from django.db import models
from accounts.models import User

class Article(models.Model):
    doi                   = models.CharField(max_length=64)
    article_file_location = models.FilePathField()

    def __unicode__(self):
        """
        return the doi of the Article object.

        @type self: Article
        @param self: the Article object itself
        @rtype: str
        @return the doi of the article.
        """
        return self.doi

    class Meta:
        db_table = "mitagger_articles"

class TagType(models.Model):
    tag_type = models.CharField(max_length=64)
    def __unicode__(self):
        """
        return the type name of the TagType object

        @type self: TagType
        @param self: the TagType object itself
        @rtype: str
        @return the type name of the TagType
        """
        return self.tagType

    class Meta:
        db_table = "mitagger_tag_types"

class Ontology(models.Model):
    virtual_id = models.CharField(max_length=128)
    name       = models.CharField(max_length=256)

    def __unicode__(self):
        """
        return the name of the ontology

        @type self: Ontology
        @param self: Ontology object itself
        @rtype: str
        @return the name of the ontology
        """
        return self.name

    class Meta:
        db_table = "mitagger_ontologies"

class Image(models.Model):
    file_name           = models.CharField(max_length=64, unique=True, null=True)
    title               = models.TextField(default=None, null=True)
    legend              = models.TextField(default=None, null=True)
    doi_link            = models.CharField(max_length=256, default=None, null=True)
    label               = models.CharField(max_length=16, default=None, null=True)
    relevant_articleDOI = models.CharField(max_length=128, default=None, null=True)
    article             = models.ForeignKey(Article, null=True)
    image_file_location = models.FilePathField(null=True)
    tag_maker           = models.ForeignKey(User, null=True)
    is_tagged           = models.BooleanField(default=False)
    is_tagging          = models.BooleanField(default=False)

    def __unicode__(self):
        """
        Return the name of the Image object.

        @type self: Image
        @param self: the Image object itself
        @rtype: string
        @return the name of the image.
        """
        return self.file_name

    class Meta:
        db_table = "mitagger_images"

class Tag(models.Model):
    id       = models.AutoField(primary_key=True)
    name     = models.CharField(max_length=2048)
    tag_type = models.ForeignKey(TagType)
    fullId   = models.CharField(max_length=128)
    ontology = models.ForeignKey(Ontology)
    #parentId = models.CharField(max_length=1024)

    def __unicode__(self):
        """
        return the name of the Tag object.

        @type self: Tag
        @param self: the Tag object itself
        @rtype: str
        @return the name of the tag
        """
        return self.fullId

    class Meta:
        db_table = "mitagger_tags"

class TagRelationship(models.Model):
    name   = models.CharField(max_length = 128)
    tag    = models.ForeignKey(Tag,related_name="tagId")
    parent = models.ForeignKey(Tag,related_name="parentId")

    def __unicode__(self):

        return self.name

    class Meta:
        db_table = "mitagger_tag_relationship"
        unique_together = ("tag","parent")


class ImageTag(models.Model):
    image           = models.ForeignKey(Image)
    tag             = models.ForeignKey(Tag)
    author          = models.ForeignKey(User)
    annotationsfrom = models.PositiveSmallIntegerField(max_length=16, default=0)
    annotationsto   = models.PositiveSmallIntegerField(max_length=16, default=0)
    matchType       = models.CharField(max_length=16, default='null')
    isSelected      = models.BooleanField(default=False)

    def __unicode__(self):
        """
        return the count of image tag

        @type self: ImageTag
        @param self: ImageTag Object itself
        @rtype: int
        @return the count of image_tags
        """

        return self.count()

    class Meta:
        db_table = "mitagger_image_tags"
        unique_together = ('image', 'tag', 'author')

class Annotations(models.Model):
    tag             = models.ForeignKey(Tag)
    keyword         = models.CharField(max_length=999)
    annotationsfrom = models.PositiveSmallIntegerField(max_length = 16)
    annotationsto   = models.PositiveSmallIntegerField(max_length = 16)
    matchType       = models.CharField(max_length = 16)
    def __unicode__(self):

        return self.keyword+","+str(self.tag.id)

    class Meta:
        db_table = "mitagger_annotations"

class ImageAssignments(models.Model):
    image    = models.ForeignKey(Image)
    user     = models.ForeignKey(User)
    reviewer = models.ForeignKey(User, related_name='FK_IMAGE_REVIEWER', db_column='reviewer')

    class Meta:
        db_table = "mitagger_image_assignments"

class Recommendations(models.Model):
    tag             = models.ForeignKey(Tag)
    image           = models.ForeignKey(Image)
    annotationsfrom = models.PositiveSmallIntegerField(max_length = 16 ,default = 0)
    annotationsto   = models.PositiveSmallIntegerField(max_length = 16,default = 0)
    matchType       = models.CharField(max_length = 16, default='null')
    def __unicode__(self):

        return self.figureLegend+","+str(self.tag.id)

    class Meta:
        db_table = "mitagger_recommendations"
